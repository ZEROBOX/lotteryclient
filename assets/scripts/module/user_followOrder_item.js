cc.Class({
    extends: cc.Component,

    properties: {
        labIsuse:{
            default: null,
            type: cc.Label
        },
        labMoney:{
            default:null,
            type:cc.Label
        },
        //中奖金额
        labRewardAmount:{
            default:null,
            type:cc.Label
        },

        //状态
        labState:{
            default:null,
            type:cc.Label
        },

        ndRewardIcon:{
            default:null,
            type:cc.Node
        },

        spArrow:{
            default:null,
            type:cc.Sprite
        }
    },

    // use this for initialization
    onLoad: function () {

    },

    init: function(data){
        this.labIsuse.string = data.IsuseNum+"期";
        this.labMoney.string = data.Amount+"元";
        
        if(data.WinMoney>0)
        {
            this.labRewardAmount.node.active = true;
            this.labRewardAmount.string = "中奖金额" + data.WinMoney.toString() + "元";
            this.ndRewardIcon.active = true;
            this.labState.string = "";
        }
        else
        {
            this.labState.string = data.State;
        }
        if(data.isArrow)
        {
            this.spArrow.node.active = true;
        }
        else
        {
            this.spArrow.node.active = false;
        }
    }
    
});

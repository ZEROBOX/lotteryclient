cc.Class({
    extends: cc.Component,

    properties: {
         isuseAnim:cc.Animation
    },

    // use this for initialization
    onLoad: function () {
        this.outOfWorld = cc.p(3000, 0);
        this.node.position = this.outOfWorld;
        let cbFadeOut = cc.callFunc(this.onFadeOutFinish, this);
        let cbFadeIn = cc.callFunc(this.onFadeInFinish, this);
        this.node.on('isuse-in', this.onShowIsuse, this);
        this.node.on('isuse-out', this.onHideIsuse, this);
    },

    onShowIsuse:function(){
        this.isuseAnim.play('ani_issue_in');
    },

    onHideIsuse:function(){
        var animState = this.isuseAnim.getAnimationState('ani_issue_out');
        if(animState.isPlaying)
            return;
        this.isuseAnim.play('ani_issue_out');
    },

    onIsuseEnd:function(){
        this.node.active = false;
    }
    
});

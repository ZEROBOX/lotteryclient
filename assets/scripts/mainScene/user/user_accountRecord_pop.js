const TradingType = cc.Enum({
    All: 0,//所有
    Recharge: 1,//充值
    Cash:2,//提现
});

/**
 * 帐户明细界面
*/

cc.Class({
    extends: cc.Component,

    properties: {

        recordContent:{
            default: null,
            type: cc.Node
        },

        recordPrefab:{
            default: null,
            type: cc.Prefab
        },

        spacePrefab:{
            default: null,
            type: cc.Prefab
        },

        labRedMoney:{
            default:null,
            type:cc.Label
        },

        labCashMoney:{
            default:null,
            type:cc.Label
        },

        labBalance:{
            default:null,
            type:cc.Label
        },

        _curPage:1,
        _curContent:null,
        _isNowRefreshing:false
    },

    // use this for initialization
    onLoad: function () {
        this._curPage = 1;
        this._isNowRefreshing = false;
        this._curContent = this.recordContent;
        this.infoRecord(this._curPage);
    },

    infoRecord:function(page){
        var recv = function(ret){
            ComponentsUtils.unblock();
            if (ret.Code !== 0) {
                cc.error(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{
                var arry = ret.Data;
                if(arry == 0)
                {
                    //cc.log("没有账户明细");
                    return;
                }
                
                this.labRedMoney.string = LotteryUtils.moneytoServer(ret.CouponsMoney).toFixed(2);
                this.labCashMoney.string = LotteryUtils.moneytoServer(ret.Withdraw).toFixed(2);
                this.labBalance.string = LotteryUtils.moneytoServer(ret.Balance).toFixed(2);

                for(var i=0;i<ret.Data.length;i++)
                {
                    var remarks = ret.Data[i].Remark.split('-');                    
                    if(remarks.length>0)
                    {
                        var time = ret.Data[i].Time;
                        var years = time[0]+time[1]+time[2]+time[3];
                        var months = time[4]+time[5];
                        var days = time[4]+time[5] +"/"+ time[6]+time[7];
                        var times = "时间："+ time[8]+time[9]+ ":" +time[10]+time[11]+":"+time[12]+time[13];
                        var ishow = false;
                        if(this._cashTemp == "")
                        {
                            this._cashTemp = months + days;
                            ishow = true;
                        }
                        else
                        {
                            if(this._cashTemp == months + days)
                            {
                                ishow = false;
                            }
                            else
                            {
                                this._cashTemp = months + days;
                                ishow = true;
                            }
                        }

                        if(ishow && i != 0 )
                        {
                            var item1 = cc.instantiate(this.spacePrefab);
                            this._curContent.addChild(item1);
                        }

                        var item = cc.instantiate(this.recordPrefab);
                        item.getComponent(item.name).init({
                            whereDce: remarks[0],
                            where:remarks[1],
                            Money:LotteryUtils.moneytoServer(ret.Data[i].Amount).toFixed(2),
                            year:years,
                            day:days,
                            time:times,
                            isShow:ishow,
                        });  
                        this._curContent.addChild(item);   

                    }
                }
               
                this._curPage++;
            }
            this._isNowRefreshing = false;
        }.bind(this);

        var data = {
            Token: User.getLoginToken(),
            UserCode:  User.getUserCode(),
            PageNumber:page,
            RowsPerPage:11,
        };
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.GETDEALRECORDLIST, data, recv.bind(this),"POST");  
        ComponentsUtils.block();
    },


    resetRefresh:function(){
        this._curContent.removeAllChildren();
        this._curPage = 1;
        this._isNowRefreshing = false;
        this._cashTemp = "";
        this.infoRecord(this._curPage);
    },

    //滚动获取下一页
    scrollCallBack: function (scrollview, eventType) {
        if(eventType === cc.ScrollView.EventType.BOUNCE_BOTTOM)
        {
            var offset_y = scrollview.getScrollOffset().y;
            var max_y = scrollview.getMaxScrollOffset().y; 
            if(offset_y - max_y>200){
                if(this._isNowRefreshing == false){
                    //cc.log("账户明细列表：下一页");
                    this._isNowRefreshing = true;
                    this.infoRecord(this._curPage);
                } 
            }
        }
    },

    //下拉刷新
    scrollRefresh: function (scrollview, eventType, customEventData) {
        if(eventType === cc.ScrollView.EventType.BOUNCE_TOP)
        {
            cc.log("账户明细列表：BOUNCE_TOP");
            var offset_y = scrollview.getScrollOffset().y;
            var max_y = scrollview.getMaxScrollOffset().y; 
            if(max_y - offset_y >200){
                //cc.log("账户明细列表：刷新：");
                this.resetRefresh();
            }
        }
    },

    onClose:function(){
        this.node.getComponent("Page").backAndRemove();
    },

});

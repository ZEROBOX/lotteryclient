/**
 * 解除绑定银行卡
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labLastBankNum:{
            default: null,
            type: cc.Label
        },

        labBankName:{
            default: null,
            type: cc.Label
        },

        _data:null,
        _bankCode:"",
        callBack:null,//回调
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.labLastBankNum.string = this._data.last;
            this.labBankName.string = this._data.bankname;
            this._bankCode = this._data.bankcode;
        }
    },

    init:function(ret,callback){
        this._data = ret;
        this.callBack = callback;
    },

    onTrue:function(){
        var recv = function(ret){
            ComponentsUtils.unblock();
            if(ret.Code != 0 && ret.Code != 49)
            {
                ComponentsUtils.showTips(ret.Msg);
                console.log(ret.Msg);
            }
            else
            {
                ComponentsUtils.showTips("解绑成功！");
                this.callBack("");
                this.onClose();
            }
        }.bind(this);          

        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
            BankCode:this._bankCode,
        }
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.UNBINDBANKCARD, data, recv.bind(this),"POST");     
        ComponentsUtils.block();
    },

    onClose:function(){
        this.node.getComponent("Page").backAndRemove();
    },

});

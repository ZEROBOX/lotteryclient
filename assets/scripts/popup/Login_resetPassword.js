/*
密码重置加密
*/
cc.Class({
    extends: cc.Component,

    properties: {
        edPassword: {
            default: null,
            type: cc.EditBox
        },

        edPasswordNew: {
            default: null,
            type: cc.EditBox
        },
 
        _verifyCode:"",
        _account:"",
    },

    // use this for initialization
    onLoad: function () {

    },

    initData: function(data){
        this._verifyCode = data.password;
        this._account = data.account;
    },

    onTrue: function(){
        if(Utils.checkInput(null,this.edPassword.string,null,this.edPasswordNew.string,true))
        {
            ComponentsUtils.block();
            var self = this;
            var recv = function(ret){
                ComponentsUtils.unblock();
                if (ret.Code !== 0) {
                    cc.error(ret.Msg);
                    ComponentsUtils.showTips(ret.Msg);
                }else{
                    window.Notification.emit("onPasswordSuccess",data);
                    this.onClose();
                }
            }.bind(this);
            
            var data = {
                Token: User.getLoginToken(),
                Mobile: this._account,
                Pwd: this.edPassword.string,
                VerifyCode:this._verifyCode,
            };
            CL.HTTP.sendRequestRet("/api/user/resetsigninpwd", data, recv.bind(this),"POST");  
        }
    },

    onTextChangePwd:function(text, editbox, customEventData){
        var txt = text;
        if(txt == "")
        {
            this.pwd = txt;
        }
        else
        {
            var re = txt.replace(/\s+/g,"");
            editbox.string = re;
            txt = re;
            this.pwd = txt;
        }
    },

    onTextChangePwd1:function(text, editbox, customEventData){
        var txt = text;
        if(txt == "")
        {
            this.pwd1 = txt;
        }
        else
        {
            var re = txt.replace(/\s+/g,"");
            editbox.string = re;
            txt = re;
            this.pwd1 = txt;
        }
    },

    onClose: function(){
         this.node.getComponent("Page").backAndRemove();
    }

});

var PageEffect = {
    Normal : 0,//不带动作展开
    TransitionSlideInR : 1,//从右往左展开
    TransitionProgressInOut : 2,//从里面向外扩展变大展开
}

cc.Class({
    extends: cc.Component,

    properties: {
        pageEffectType:0,//page的特效类型 默认0：直接直接展开 
        pageType:1,//层级类型 默认1为普通界面 0为主界面
        isChatPage:false,//是否聊天室
    },

    // use this for initialization
    onLoad: function () {
        if(this.pageType != 0)
        {
            this.node.on(cc.Node.EventType.TOUCH_START, function (event) {
                 
            }.bind(this), this);
            this.node.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
                  
            }.bind(this), this);
        }   

        //相同界面唯一
        var canvas = cc.find("Canvas");
        var canvasChildren = canvas.children;
        var count = 0;
        for(var i = 0; i<canvasChildren.length; i++){
            if(canvasChildren[i].name == this.node.name)
            {
                //cc.log("[page] uid1:"+canvasChildren[i].uuid+" uid2:"+this.node.uuid );
                count++;
                if(count == 2)
                {
                    //cc.log("【page】该界面已存在-remove:" + this.node.name);
                    this.node.destroy();
                    return;
                }
            }
        }

        switch (this.pageEffectType)
        {
            case PageEffect.Normal:
            {
                //cc.log("直接显示");
            }
            break;
            case PageEffect.TransitionSlideInR:
            {
                this.node.runAction(cc.sequence(cc.delayTime(0.01), cc.callFunc(function(){
                    this.appear();        
                }.bind(this))));
            }
            break;
            case PageEffect.TransitionProgressInOut:
            {
                var content = this.node.getChildByName("content");
                this.actionScaleIn = cc.sequence(cc.scaleTo(0.3, 1.05, 1.05).easing(cc.easeCubicActionOut()),cc.scaleTo(0.1, 1, 1).easing(cc.easeCubicActionOut()));
                content.scale = 0.5;   
                content.runAction(this.actionScaleIn);
            }
            break;
            default:
            break;
        }

        var listener = {
            event: cc.EventListener.KEYBOARD,
            onKeyReleased: function (keyCode, event) {
                //cc.log("onKeyReleased in page");
                if(keyCode===cc.KEY.back){//返回键
                    var canvas = cc.find("Canvas");
                    var canvasChildren = canvas.children;
                    var highestZOrder = 0;
                    for(var i = 0; i<canvasChildren.length; i++){
                        if(canvasChildren[i].getLocalZOrder()>=highestZOrder && canvasChildren[i].getLocalZOrder()<40000){
                            highestZOrder = canvasChildren[i].getLocalZOrder();
                        }
                        else
                        {
                            return;
                        }
                    }
                    //cc.log("highestZOrder:"+highestZOrder+",getLocalZOrder:"+this.node.getLocalZOrder());
                    if(highestZOrder == this.node.getLocalZOrder()){
                        if(highestZOrder == 0)
                        {
                            this.onGameOver();
                        }
                        else
                        {
                       //     cc.log("page node.name:" + this.node.name);
                            if(this.node.getComponent(this.node.name) != null)
                            {
                                if(this.isChatPage)
                                {
                                    var betPanel = this.node.getChildByName("content").getChildByName("BetContent").getChildByName("BetPanelContent");
                                    var content = betPanel.getChildByName("BetScrollView").getChildByName("view").getChildByName("content");

                                    if(betPanel.active == true && content !=null && content.children.length > 0)
                                    {
                                        if(content.children[0].getComponent(content.children[0].name) != null )
                                            content.children[0].getComponent(content.children[0].name).onClose();
                                    }
                                    else
                                    {
                                        this.node.getComponent(this.node.name).onClose();
                                    }
                                }
                                else
                                {
                                    this.node.getComponent(this.node.name).onClose();
                                }
                            }
                            else
                            {
                                this.backAndRemove();
                            } 
                        }
                    } 
                }
            }.bind(this)
        };

        // 绑定键盘事件
        cc.eventManager.addListener(listener, this.node);
        
        this.node.runAction(cc.sequence(cc.delayTime(0.01),cc.callFunc(function(){
            if(this.pageType == 0)
            {
                this.node.setLocalZOrder(0); 
                //cc.log("page当前界面层级：" + this.node.name+ "->"+this.node.getLocalZOrder());   
            }
            else if(this.pageType == 1)
            {
                this.node.setLocalZOrder(ComponentsUtils.getPanelTag()); 
                //cc.log("page当前界面层级：" + this.node.name+ "->"+this.node.getLocalZOrder());   
            }

        }.bind(this))));
    },

    appear:function(){
        var content = this.node.getChildByName("content");
        if(content == null) return;
        content.setPosition(1080,content.y);
        content.runAction(cc.sequence(cc.moveTo(0.15, cc.p(0, content.y)), cc.callFunc(function(){
            this.isAppear = true;
        }.bind(this))));
    },

    backAndRemove:function(){
        var node = this.node;
        switch (this.pageEffectType)
        {
            case PageEffect.Normal:
            {
                this.node.destroy();
            }
            break;
            case PageEffect.TransitionSlideInR:
            {
                var screenSize = cc.view.getVisibleSize();
                this.node.runAction(cc.sequence(cc.moveTo(0.15, cc.p(screenSize.width, this.node.y)), cc.callFunc(function(){
                    this.node.destroy();
                }.bind(this),this)));
            }
            break;
            case PageEffect.TransitionProgressInOut:
            {
                this.node.destroy();
            }
            break;
            default:
            break;
        }
    },

    onGameOver:function(){    
        var bEnd = false;
        var confirmCallback = function(){
            if(bEnd)
            {
                // cc.log("game END Ret");
                return;
            }
            bEnd = true;
            cc.director.end(); 
            //cc.game.end();
        }.bind(this);
        var closeCallback = function(){
            
        }.bind(this);
        ComponentsUtils.showAlertTips(2, "确定是否退出彩乐", closeCallback, "是否退出彩乐", confirmCallback);    
    }

});

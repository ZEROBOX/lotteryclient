(function(){ 
    var _userCode = -1;//用户id
    var _pwd = "";//用户名密码
    var _name = "游客";//用户名，昵称
    var _fullName = "";//真实姓名
    var _tel = "";//手机号
    var _balance = 0; // 可用余额 double
    var _goldBean = 0;//彩豆
    var _isrobot;//是否机器人
    var _avataraddress = "";//头像地址
    var _isvermify = false;//是否验证通过，false否，true是
    var _isbindtel;//是否绑定手机，false否，true是
    var _loginToken = "";//身份识别令牌
    var _loginMode = "1";//登录模式 1：游客登录 2：手机号登录 3：微信登录 4：QQ登录 5：其他
    var _vipLevel = 0;//vip等级
    var _isCertification = false;//是否实名认证，false否，true是
    var _isWithdrawPwd = false;//是否有提现密码
    var _userUid = "";//用户uid

    var _coupons;//优惠卷 (原红包) 
    var _freeze; // 冻结金额 double
    var _recommenderid;//推荐人id
    var _accid = "";//云信id
    var _token = "";//云信登录标识
    var _idols;//关注数
    var _fans;//粉丝数
    var _isFirstLogin = true;
    var _pushTokenId = "";//百度云推送id

    var _appState = 1;//app状态
    
    var _smallGames = [];//小游戏列表

    var telNoTem = cc.sys.localStorage.getItem('telNo');
    var pwdTem = cc.sys.localStorage.getItem('pwd');
    var loginMode = cc.sys.localStorage.getItem('loginMode');
    var loginToken = cc.sys.localStorage.getItem('loginToken');
    var uid = cc.sys.localStorage.getItem('uid');

    if(typeof uid == "string"){
        _setUid(uid);
    }else{
        // cc.log("uid not a string");
    }

    if(typeof loginToken == "string"){
        _setLoginToken(loginToken);
    }else{
        // cc.log("loginToken not a string");
    }

    if(typeof loginMode == "string"){
        _setLoginMode(loginMode);
    }else{
        // cc.log("loginMode not a string");
    }

    if(typeof telNoTem == "string"){
        _setTel(telNoTem);
    }else{
        // cc.log("telNoTem not a string");
    }
    if(typeof pwdTem == "string"){
        _setPwd(pwdTem);
    }else{
        // cc.log("pwdTem not a string");
    }

    function _setSmGame(data){
        _smallGames = data
    }

    function _getSmGame(index){
        return _smallGames[index-1];
    }

    function _getAppState() {
        return  _appState;
    }
    
    function _setAppState(content) {
        if(content == null){
            content = false;
        }
        _appState = content;
    }

    function _getUid() {
        return  _userUid;
    }
    
    function _setUid(content) {
        if(content == null){
            content = "";
        }
        _userUid = content;
        cc.sys.localStorage.setItem('uid',_userUid);
    }

    function _getIsCertification() {
        return  _isCertification;
    }
    
    function _setIsCertification(content) {
        if(content == null){
            content = false;
        }
        _isCertification = content;
    }

    function _getVipLevel() {
        return  _vipLevel;
    }
    
    function _setVipLevel(content) {
        if(content == null){
            content = 0;
        }
        _vipLevel = content;
    }

    function _getLoginMode() {
        return  _loginMode;
    }
    
    function _setLoginMode(content) {
        if(content == null){
            content = "1";
        }
        _loginMode = content;
        cc.sys.localStorage.setItem('loginMode',_loginMode);
    }

    function _getuserCode() {
        return  _userCode;
    }
    
    function _setUserCode(content) {
        if(content == null){
            content = -1;
        }

        _userCode = content;
        if(_userCode>0 && _loginToken!="" && _pushTokenId != "")
        {
            var recv = function recv(ret) {
                if (ret.Code !== 0) {
                } else {
                    // cc.log("推送ID上传成功");
                }
            }.bind(this);
            var data = {
                PushIdentify:_pushTokenId,
                UserCode: _userCode,
                Token:_loginToken
            };
            CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.SETBAIDUID, data, recv.bind(this),"PUT"); 
        }
    }

    function _getFullName(){
        return _fullName;
    }
    function _setFullName(content){
        if(content == null){
            content = "";
        }
        _fullName = content;
    }   
    function _getPwd() {
        return  _pwd;
    }
    
    function _setPwd(content) {
        if(content == null){
            content = "";
        }
        cc.sys.localStorage.setItem('pwd', content);
        _pwd = content;
    }
    function _getNickName() {
        return  _name;
    }
    
    function _setNickName(content) {
        if(content == null){
            content = "游客";
        }
        _name = content;
    }
    function _getTel() {
        return  _tel;
    }
    
    function _setTel(content) {
        if(content == null){
            content = "";
        }
        
        cc.sys.localStorage.setItem('telNo', content);
        _tel = content;
        if(_tel != "")
            _setIsbindtel(true);
        else
            _setIsbindtel(false);     
    }
    function _getBalance() {
        return  _balance;
    }
    
    function _setBalance(content) {
        _balance = content/100;
    }
    function _getCoupons() {
        return  _coupons;
    }
    
    function _setCoupons(content) {
        
        _coupons = content;
    }
    function _getGoldBean() {
        return  _goldBean;
    }
    
    function _setGoldBean(content) {
        _goldBean = content;
    }
    
    function _getFreeze() {
        return  _freeze;
    }
    
    function _setFreeze(content) {
        _freeze = content;
    }
    function _getRecommenderid() {
        return  _recommenderid;
    }
    
    function _setRecommenderid(content) {
        if(content == null){
            content = "";
        }
        _recommenderid = content;
    }
    function _getAccid() {
        return  _accid;
    }
    
    function _setAccid(content) {
        if(content == null){
            content = "";
        }
        _accid = content;
    }
    function _getToken() {
        return  _token;
    }
    
    function _setToken(content) {
        if(content == null){
            content = "";
        }
        _token = content;
    }

    function _getIsrobot() {
        return  _isrobot;
    }
    
    function _setIsrobot(content) {
        _isrobot = content;
    }
    function _getIdols() {
        return  _idols;
    }
    
    function _setIdols(content) {
        _idols = content;
    }
    function _getFans() {
        return  _fans;
    }
    
    function _setFans(content) {
        _fans = content;
    }
    function _getAvataraddress() {
        return  _avataraddress;
    }
    
    function _setAvataraddress(content) {
        if(content == null){
            content = "";
        }
        _avataraddress = content;
    }
    function _getIsvermify() {
        return  _isvermify;
    }
    
    function _setIsvermify(content) {
        if(content == null){
            content = false;
        }
        _isvermify = content;
    }
    function _getIsbindtel() {
        return  _isbindtel;
    }
    
    function _setIsbindtel(content) {
        if(content == null){
            content = false;
        }
        _isbindtel = content;
    }
    function _getIsLogin() {
        return  _isLogin;
    }
    
    function _setIsLogin(content) {
        _isLogin = content;
    }
    function _setLoginToken(content){
        if(content == null){
            content = "";
        }
        _loginToken = content;
        cc.sys.localStorage.setItem('loginToken',_loginToken);
    }
    function _getLoginToken(content){
        return _loginToken;
    }
    function _setIsFirstLogin(content){
        _isFirstLogin = content;
    }
    function _getIsFirstLogin(){
        return _isFirstLogin;
    }
    function _setPushTokenId(content){
        if(content == null){
            content = "";
        }
        _pushTokenId = content;
    }
    function _getPushTokenId(){
        return _pushTokenId;
    }
    function _setIsWithdrawPwd(content){

        _isWithdrawPwd = content;
    }

    function _getIsWithdrawPwd(){
       return _isWithdrawPwd;
    }

    //重置列表
    function _upDateUserInfo(isReset,data){
        if(isReset)
        {
            _setUserCode(-1);
            _setNickName(null);
            _setTel("");
            _setBalance(0);
            _setGoldBean(0);
            _setIsrobot(false);
            _setAvataraddress("");
            _setVipLevel(0);
            _setFullName("");
            _setIsCertification(false);
            _setIsWithdrawPwd(false);//提现密码状态
        }
        else
        {
            try {
                _setUserCode(data.UserCode);
                _setNickName(data.Nick);
                _setTel(data.Mobie);
                _setBalance(data.Balance);
                _setGoldBean(data.Gold);
                _setIsrobot(data.IsRobot);
                _setAvataraddress(data.AvatarUrl);
                _setVipLevel(data.VIP);
                _setFullName(data.FullName);
                _setIsCertification(data.IsCertification);
                _setIsWithdrawPwd(data.IsWithdrawPwd);//提现密码状态
                _setGoldBean(data.Gold);
            } catch (error) {
                // cc.log("更新用户数据失败 "+error);
            }
        }
    }

var _User={
        upDateUserInfo:_upDateUserInfo,
        getUserCode:_getuserCode,
        setUserCode:_setUserCode,
        getPwd:_getPwd,
        setPwd:_setPwd,
        getNickName:_getNickName,
        setNickName:_setNickName,
        getTel:_getTel,
        setTel:_setTel,
        getBalance:_getBalance,
        setBalance:_setBalance,
        getCoupons:_getCoupons,
        setCoupons:_setCoupons,
        getGoldBean:_getGoldBean,
        setGoldBean:_setGoldBean,
        getFreeze:_getFreeze,
        setFreeze:_setFreeze,
        getRecommenderid:_getRecommenderid,
        setRecommenderid:_setRecommenderid,
        getAccid:_getAccid,
        setAccid:_setAccid,
        getToken:_getToken,
        setToken:_setToken,
        getIsrobot:_getIsrobot,
        setIsrobot:_setIsrobot,
        getIdols:_getIdols,
        setIdols:_setIdols,
        getFans:_getFans,
        setFans:_setFans,
        setAvataraddress:_setAvataraddress,
        getAvataraddress:_getAvataraddress,
        setIsvermify:_setIsvermify,
        getIsvermify:_getIsvermify,
        setIsbindtel:_setIsbindtel,
        getIsbindtel:_getIsbindtel,
        getIsLogin:_getIsLogin,
        setIsLogin:_setIsLogin,
        getLoginToken:_getLoginToken,
        setLoginToken:_setLoginToken,
        setIsFirstLogin:_setIsFirstLogin,
        getIsFirstLogin:_getIsFirstLogin,
        setPushTokenId:_setPushTokenId,
        getPushTokenId:_getPushTokenId,
        setFullName:_setFullName,
        getFullName:_getFullName,
        setLoginMode:_setLoginMode,
        getLoginMode:_getLoginMode,
        setVipLevel:_setVipLevel,
        getVipLevel:_getVipLevel,
        setIsCertification: _setIsCertification,
        getIsCertification: _getIsCertification,
        getIsWithdrawPwd:_getIsWithdrawPwd,
        setIsWithdrawPwd:_setIsWithdrawPwd,
        getAppState:_getAppState,
        setAppState:_setAppState,
        getUid:_getUid,
        setUid:_setUid,

        //小游戏列表信息
        setSmGame:_setSmGame,
        getSmGame:_getSmGame,
   }
   
   window.User=_User; 
    
} 
)();



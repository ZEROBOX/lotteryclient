(function(){ 

    function _addScrollEffect(target,spacey,index){
        var wHeight = 0;
        var wWidth = 0;
        for(let i=0;i<target.children.length;i++)
        {
            var oldx = target.children[i].x;
            var posx = 0-target.children[i].width;
            var posy = 0-target.children[i].height *i + spacey;
            target.children[i].setPosition(posx,posy);
            target.children[i].active = true;
            var actionBy = cc.moveTo(0.1, cc.p(oldx, posy));
            cc.speed(actionBy, 2);

            wHeight+=target.children[i].height;
            wWidth=target.children[i].width;
    
            if(i==0 && i>=index)
            {
                cc.log("widget1 i:"+i + " x:"+oldx+"y:"+posy);
                target.children[i].runAction(actionBy);
            }
            else 
            {
                if(i>=index)
                {
                    cc.log("widget2 i:"+i+" x:"+oldx+"y:"+posy);
                    var time = i/10;
                    target.children[i].runAction(cc.sequence(cc.delayTime(time), actionBy));
                }
            }
        }

        target.height = wHeight;
        target.width = wWidth;
        cc.log("widget content "+wWidth+" "+wHeight);
    }
    
    var _WidgetExtUtils = {
        addScrollEffect:_addScrollEffect
    };
    window.WidgetExtUtils = _WidgetExtUtils;
} 
)();

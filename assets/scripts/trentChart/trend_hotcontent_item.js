/**
 * !#zh 走势图冷热组件
 * @information 号码/和值,30期，50期，100期，遗漏
 */
cc.Class({
    extends: cc.Component,

    properties: {
        number: cc.Node,
        Issue30: cc.Label,
        Issue50: cc.Label,
        Issue100: cc.Label,
        miss: cc.Label,
        SpFrame: [cc.SpriteFrame],
        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.node.color = this._data.color;
            this.show();
        }
    },

    /** 
    * 接收走势图冷热信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;  
    },

    /** 
    * 刷新走势图冷热信息
    * @method updateData
    * @param {Object} data
    */
    updateData:function(data){
        this._data = data;
        if(data != null)
        {
            this.node.color = this._data.color;
            this.show();
        }
    },

    /** 
    * 显示走势图冷热信息
    * @method show
    * @param {} 
    */
    show: function(){
        if(this._data.type ==1){
            this.number.getChildByName('numLab').getComponent(cc.Label).string = this._data.num;
            this.number.getComponent(cc.Sprite).spriteFrame =this.SpFrame[0];
        }else if(this._data.type ==2){        
            //var texture =cc.textureCache.addImage(cc.url.raw("resources/textures/chat/chat_ball_blue.png"))
            this.number.getComponent(cc.Sprite).spriteFrame =this.SpFrame[1];
            this.number.getChildByName('numLab').getComponent(cc.Label).string = this._data.num; 
        }else if(this._data.type ==3){
             this.number.getComponent(cc.Sprite).enabled =false;
             this.number.getChildByName('numLab').color = new cc.Color(124, 121, 126);
             this.number.getChildByName('numLab').getComponent(cc.Label).string = this._data.num;
        }
        this.Issue30.string = this._data.issue30;
        this.Issue50.string = this._data.issue50;
        this.Issue100.string = this._data.issue0;
        this.miss.string = this._data.miss;    
    }

});

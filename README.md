# 彩乐彩票客户端

![输入图片说明](https://gitee.com/uploads/images/2018/0119/105616_b74a1d3c_1700303.png "caile-lottery.png")

彩乐彩票是国内第一个免费，开源的完整的彩票实时聊天+购彩解决方案。目前支持的彩种有7个：双色球，超级大乐透，山东11选5，湖北11选5，江西快三，吉林快三，重庆时时彩。其他流行的20多个彩种如竞彩足球、竞彩篮球、老足彩、北京单场、七星彩、七乐彩、福彩3D，排列3、排列5等彩种已列入开发计划中，后续将陆续开放。


项目起源

老彩民在线下购买彩票时，都会在彩票店看走势图，和周围经常买彩的彩友一起看走势图、一起讨论这一期会开什么号码，什么样的方法可以提高中奖率，竞彩彩民更是如此。同一个彩票店经常买彩票的大家都认识，时间长了形成彩民圈子。彩乐彩票就是将购买彩票、看彩票走势图、彩票专家预测、赛事分析、即时聊天、高手跟单6大场景进行整合，为广大彩友推出的集6大需求为一体的一站式即时聊天购彩社区。

1.	开发引擎说明
开发引擎：
Cocos Creator 1.5.2.
开发语言：
Javascript
开发平台：
MAC,Windows
可发布平台: IOS,Android,Windows,Mac,Html5

2.	客户端主流程图

 ![输入图片说明](https://gitee.com/uploads/images/2018/0119/105650_b364cb13_1700303.png "caile-lottery-client.png")


